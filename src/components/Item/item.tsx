import React, { useContext, useState } from 'react'
import { Product } from '../../types/item'
import './item.css'
import { Context } from '../../Context/AppContext'
import { Link } from 'react-router-dom'
import { formatter } from '../../utils/utils'
import BeatLoader from 'react-spinners/BeatLoader'

interface ItemProps {
  item: Product
}
export function Item(props: ItemProps): React.ReactElement {
  const { addToCart, cartItems } = useContext(Context)
  const [loading, setLoading] = useState(false)

  const handleCartAdd = () => {
    setLoading(true)
    setTimeout(() => {
      addToCart(props.item)
      setLoading(false)
    }, 500)
  }

  return (
    <div className="flex flex-col my-2">
      <Link
        to={`/product-details/${props.item._id}/${props.item.name}`}
        style={{ textDecoration: 'none' }}
      >
        <div className="item-container">
          <img
            className="item-attribute item-image"
            src={props.item.imageURL}
          />
          <div className="item-attribute item-title">
            <strong>Item:</strong> {props.item.name}
          </div>
          <div className="item-attribute item-category">
            <strong>Category:</strong> {props.item.category}
          </div>
          <div className="item-attribute item-price">
            <strong>Quantity Available: </strong>
            {props.item.quantity}
          </div>
          <div className="item-attribute item-price">
            <strong>Price: </strong>
            {formatter.format(props.item.price)}
          </div>
        </div>
      </Link>
     {props.item.quantity>0? (<button
        className={
          'rounded-lg px-4 py-2 text-white bg-[#004d8e] w-[200px] self-center mt-auto cursor-pointer'
        }
        onClick={handleCartAdd}
      >
        {' '}
        {loading ? (
          <BeatLoader size={12} color="white"></BeatLoader>
        ) : (
          'Add to Cart'
        )}{' '}
      </button>): <div className='text-red-400 font-bold'>Out of Stock</div>}
    </div>
  )
}
