import { AxiosResponse } from 'axios'
import { useContext, useEffect, useState } from 'react'
import { Context } from '../../Context/AppContext'
import { Product } from '../../types/item'
import Axios from 'axios'
import { Item } from '../Item/item'
import ScaleLoader from 'react-spinners/ScaleLoader'
import './Home.css'
import { productsURL } from '../../constants/constants'
import { useNavigate } from 'react-router-dom'

export function Home() {
  const [allItems, setAllItems] = useState<Product[]>([])
  const [loading, setLoading] = useState(true)
  const navigate = useNavigate();

  const loadAllItems = () => {
    return allItems.map((i: Product) => <Item key={i._id} item={i} />)
  }

  useEffect(() => {
    setLoading(true)
    Axios.get(productsURL)
      .then(function (response: AxiosResponse<any>) {
        // handle success
        setAllItems(response.data)
      })
      .catch(function (error: any) {
        // handle error
        console.log(error)
        navigate('/error');
      })
      .finally(() => {
        setLoading(false) // This will be executed regardless of success or error
      })
  }, [])
  if (loading) {
    return (
      <div className="spinner-container">
        <ScaleLoader color="#004d8e" />
      </div>
    )
  } else {
    return <div className="main-container">{loadAllItems()}</div>
  }
}
