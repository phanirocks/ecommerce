import { AiOutlineShoppingCart } from 'react-icons/ai/index'
import { AiFillDelete } from 'react-icons/ai/index'
import '../Cart/Cart.css'

import React, { useContext, useEffect, useState } from 'react'
import { Context } from '../../Context/AppContext'
import { formatter } from '../../utils/utils'
import { useNavigate, useParams } from 'react-router-dom'
import Axios, { AxiosResponse } from 'axios'
import { ordersURL } from '../../constants/constants'
import { Order } from '../../types/order'
import ScaleLoader from 'react-spinners/ScaleLoader'

export function OrderPage() {
  const { cartItems, removeFromCart, totalPrice } = useContext(Context)
  const { id } = useParams()
  const [loading, setLoading] = useState(true)
  const [order, setOrder] = useState<Order>()
  const navigate = useNavigate();

  useEffect(() => {
    setLoading(true)
    Axios.get(ordersURL + id)
      .then(function (response: AxiosResponse<Order>) {
        // handle success
        setOrder(response.data)
        setLoading(false)
      })
      .catch(function (error: any) {
        // handle error
        console.log(error)
        navigate('/error');
      })
  }, [])

  function arrangeOrderItems() {
    return (
      <div className="flex flex-col gap-2">
        <div>
          <strong>Order Date: </strong>
          {order?.orderDate}
        </div>
        <div>
          <strong>Order Status:</strong> {order?.status}
        </div>
        <div>
          <strong>Order Total Price:</strong> {order?.totalPrice}
          {order?.currency}
        </div>
        <div>
          <strong>Customer Email Address:</strong> {order?.email}
        </div>
        <div>
          <strong>Customer Address:</strong> {order?.address}
        </div>
        <div>
          <strong>Payment Method:</strong> {order?.paymentMethod}
        </div>
        <div>
          <strong>Shipping Method:</strong> {order?.shippingCompany}
        </div>
        <div>
          <strong>Tracking Number:</strong> {order?.shippingTrackingNumber}
        </div>
      </div>
    )
  }

  if (loading) {
    return (
      <div className="spinner-container">
        <ScaleLoader color="#004d8e" />
      </div>
    )
  } else {
    return (
      <div className="flex align-center flex-col justify-center">
        <h2>Your Order has ben placed successfully. OrderID is {order?._id}</h2>
        {arrangeOrderItems()}
      </div>
    )
  }
}
