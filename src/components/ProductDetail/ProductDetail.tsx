import React, { useContext, useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { Context } from '../../Context/AppContext'
import './ProductDetail.css'
import { formatter } from '../../utils/utils'
import { Product } from '../../types/item'
import Axios, { AxiosResponse } from 'axios'
import ScaleLoader from 'react-spinners/ScaleLoader'
import BeatLoader from 'react-spinners/BeatLoader'
import { productsURL } from '../../constants/constants'

export function ProductDetail(): React.ReactElement {
  const { addToCart } = useContext(Context)
  const [loading, setLoading] = useState(true)
  const [cartLoading, setCartLoading] = useState(false)
  const [item, setItem] = useState<Product>()
  const navigate = useNavigate();

  interface ParamTypes {
    id: string
  }
  const { id } = useParams()

  useEffect(() => {
    setLoading(true)
    Axios.get(productsURL + id)
      .then(function (response: AxiosResponse<any>) {
        // handle success
        setItem(response.data)
        setLoading(false)
      })
      .catch(function (error: any) {
        // handle error
        console.log(error)
        navigate('/error');
      })
  }, [])

  function handleAddToCart(item: Product | undefined) {
    setCartLoading(true)
    setTimeout(() => {
      addToCart(item)
      setCartLoading(false)
    }, 500)
  }
  const img = item?.imageURL || ''
  const title = item?.name
  const desc = item?.description || ''
  const price = formatter.format(item?.price || 0)
  const category = item?.category
  if (loading) {
    return (
      <div className="spinner-container">
        <ScaleLoader color="#004d8e" />
      </div>
    )
  } else {
    return (
      <div className="flex flex-col md:mr-[20%]">
        <div className="product-detail-title mb-4">
          <strong>{title}</strong>
        </div>
        <div className="product-detail-container">
          <img className="product-detail-image" src={img} />
          <div>
            <div className="product-detail-description max-w-[700px] px-[800px]">{desc}</div>
            <div className="product-detail-price">
              <strong>Price: </strong>
              {price}
            </div>
            <div className="product-detail-category">
              <strong>Category: </strong>
              {category}
            </div>
            {item && item?.quantity>0? ( <button
              className="product-detail-button rounded-lg"
              onClick={() => handleAddToCart(item)}
            >
              {' '}
              {cartLoading ? (
                <BeatLoader size={12} color="white"></BeatLoader>
              ) : (
                'Add to Cart'
              )}{' '}
            </button>):<div className='text-red-400 font-bold'>Out of Stock</div>}
          </div>
        </div>
      </div>
    )
  }
}
