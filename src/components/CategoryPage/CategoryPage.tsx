import { AxiosResponse } from 'axios'
import { useContext, useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { Context } from '../../Context/AppContext'
import { Product } from '../../types/item'
import Axios from 'axios'
import { Item } from '../Item/item'
import ScaleLoader from 'react-spinners/ScaleLoader'
import { productsURL } from '../../constants/constants'

export function CategoryPage() {
  const [items, setItems] = useState<Product[]>([])
  const [loading, setLoading] = useState(true)
  const { category } = useParams()
  const navigate = useNavigate();

  const loadAllItems = () => {
    return items.map((i: Product) => <Item key={i._id} item={i} />)
  }

  useEffect(() => {
    setLoading(true)
    Axios.get(productsURL + '?category=' + category)
      .then(function (response: AxiosResponse<any>) {
        // handle success
        setItems(response.data)
      })
      .catch(function (error: any) {
        // handle error
        console.log(error)
        navigate('/error')
      })
      .finally(() => {
        setLoading(false) // This will be executed regardless of success or error
      })
  }, [category])
  if (loading) {
    return (
      <div className="spinner-container">
        <ScaleLoader color="#004d8e" />
      </div>
    )
  } else {
    return <div className="main-container">{loadAllItems()}</div>
  }
}
