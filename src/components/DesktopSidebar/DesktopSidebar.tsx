import {
  CalendarIcon,
  ChartPieIcon,
  DocumentDuplicateIcon,
  FolderIcon,
  HomeIcon,
  UsersIcon,
  ComputerDesktopIcon
} from '@heroicons/react/24/outline'
import { useNavigate } from 'react-router-dom'

const navigation = [
  { name: 'Home', href: '/', icon: HomeIcon, count: '5', current: true },
  { name: 'Electronics', href: '/category/electronics', icon: ComputerDesktopIcon, current: false },
  {
    name: 'Clothing',
   href: '/category/clothing',
    icon: UsersIcon,
    count: '12',
    current: false,
  },
]

function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(' ')
}

export default function DesktopSidebar() {
  const navigate = useNavigate()
  return (
    <nav className="flex flex-col max-w-[180px] cursor-pointer mr-auto hidden md:block" aria-label="Sidebar">
      <ul role="list" className="-mx-2 space-y-1 list-none">
        {navigation.map((item) => (
          <li key={item.name}>
            <div
              onClick={() => {
                navigate(item.href)
              }}
              className=
                   'text-gray-900 hover:text-indigo-600 hover:bg-gray-100 group flex gap-x-3 rounded-md p-2 text-sm leading-6 font-semibold text-lg'>
            
              <item.icon
                className={classNames(
                  item.current
                    ? 'text-indigo-600'
                    : 'text-gray-400 group-hover:text-indigo-600',
                  'h-6 w-6 shrink-0',
                )}
                aria-hidden="true"
              />
              {item.name}
            </div>
          </li>
        ))}
      </ul>
    </nav>
  )
}
