import React, { useState } from 'react'
import { useNavigate } from 'react-router';
import './SearchBar.css';

export function SearchBar():React.ReactElement {

    const [searchQuery, setSearchQuery] = useState<string>('');
    const navigate = useNavigate();

    return (
        <div className='search-bar-container'>
            <input className='search-bar-input w-96' onChange = {(e => setSearchQuery(e.target.value))} />
            <button className='search-bar-button' onClick={() => navigate(`/search-results/${searchQuery}`)}>Search</button>
        </div>
    );
}