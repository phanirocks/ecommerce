import { AiOutlineShoppingCart } from 'react-icons/ai/index'
import { AiFillDelete } from 'react-icons/ai/index'
import './Cart.css'

import React, { useContext, useEffect, useState } from 'react'
import { Context } from '../../Context/AppContext'
import { formatter } from '../../utils/utils'
import { CreateOrderRequest, Order } from '../../types/order'
import Axios, { AxiosResponse } from 'axios'
import { ordersURL } from '../../constants/constants'
import { useNavigate } from 'react-router-dom'
import ScaleLoader from 'react-spinners/ScaleLoader'
import BeatLoader from 'react-spinners/BeatLoader'

export function Cart() {
  const { cartItems, removeFromCart, totalPrice, emptyCart } = useContext(
    Context,
  )
  const [loading, setLoading] = useState(false)
  const [email, setEmail] = useState('')
  const [address, setAddress] = useState('')
  const [formFilled, setFormFilled] = useState(false)
  useEffect(() => {
    if (email && address) {
      setFormFilled(true)
    } else {
      setFormFilled(false)
    }
  }, [email, address])
  const navigate = useNavigate()
  const arrangeCartItems = () => {
    return cartItems.map((cartItem, index) => (
      <div className="cart-row" key={index}>
        <div className="cart-row-item">{cartItem.name}</div>
        <div className="cart-row-item">
          <strong>{formatter.format(cartItem.price)}</strong>
        </div>
        <div
          className="cart-row-item cart-row-delete"
          onClick={() => removeFromCart(index)}
        >
          {' '}
          <AiFillDelete size="20px" />
        </div>
      </div>
    ))
  }

  const placeOrder = () => {
    setLoading(true)
    const orderItems = []
    for (const cartItem of cartItems) {
      orderItems.push({
        productId: cartItem._id,
        quantity: 1,
      })
    }
    const order: CreateOrderRequest = {
      products: orderItems,
      date: new Date().toLocaleDateString(),
      totalPrice: totalPrice,
      currency: cartItems.length ? cartItems[0].currency : 'AUD',
      email: email,
      address: address,
    }

    Axios.post(ordersURL, order)
      .then(function (response: AxiosResponse<Order>) {
        // handle success
        console.log(response.data)
        emptyCart()
        navigate('/orders/' + response.data._id)
      })
      .catch(function (error: any) {
        // handle error
        console.log(error)
        navigate('/error')
      })
      .finally(() => {
        setLoading(false) // This will be executed regardless of success or error
      })
  }

  if (!cartItems.length) {
    return <div>There are currently no items in the cart.</div>
  } else {
    return (
      <div className="cart-container">
        <div>
        <div className="cart-row">
          <div>
            <strong>Item </strong>
          </div>
          <div>
            <strong> Price</strong>
          </div>
          <div></div>
        </div>
        {arrangeCartItems()}
        <div className="cart-row">
          <div></div>
          <div>
            <strong> Total Price</strong>
          </div>
          <div>
            <strong> {formatter.format(totalPrice)}</strong>
          </div>
        </div>
        </div>
        <div>
        <div className="flex flex-col gap-2 justify-center align-middle my-2 w-[300px] mt-16">
          <div>Email:</div>
          <input
            className="p-1"
            type="email"
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div className=" flex flex-col w-[300px] gap-2 justify-center align-middle my-2">
          <div>Address:</div>
          <input className="p-1" onChange={(e) => setAddress(e.target.value)} />
        </div>
        {formFilled ? (
          <button
            className="pay-button cursor-pointer"
            onClick={() => placeOrder()}
          >
            {loading ? (
              <BeatLoader size={12} color="white"></BeatLoader>
            ) : (
              'Place Order'
            )}
          </button>
        ) : (
           <button
            className="pay-button cursor-pointer focus:outline-none disabled:opacity-25"
            disabled
            onClick={() => placeOrder()}
          >
            {loading ? (
              <BeatLoader size={12} color="white"></BeatLoader>
            ) : (
              'Place Order'
            )}
          </button>
        )}
        </div>
      </div>
    )
  }
}
