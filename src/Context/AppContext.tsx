import React, {
  useState,
  ReactChild,
  ReactChildren,
  useEffect,
  ReactNode,
} from 'react'
import { AxiosResponse } from 'axios'
import { Product } from '../types/item'
import Axios from 'axios'

type ContextType = {
  allItems: Array<Product>
  setAllItems: Function
  cartItems: Array<Product>
  emptyCart: Function
  loading: boolean
  setLoading: Function
  addToCart: Function
  removeFromCart: Function
  totalPrice: number
}

const defaultValue: ContextType = {
  allItems: [],
  setAllItems: () => {},
  cartItems: [],
  emptyCart: () => {},
  loading: false,
  setLoading: () => {},
  addToCart: () => {},
  removeFromCart: () => {},
  totalPrice: 0,
}

const Context = React.createContext<ContextType>(defaultValue)

interface ContextProviderProps {
  children: ReactNode
}

function ContextProvider({ children }: ContextProviderProps) {
  const [allItems, setAllItems] = useState<Product[]>([])
  const [cartItems, setCartItems] = useState<Product[]>([])
  const [loading, setLoading] = useState(false)
  const [totalPrice, setTotalPrice] = useState(0)

  const addToCart = (item: Product) => {
    const currentCartItems = cartItems
    currentCartItems.push(item)
    setCartItems(currentCartItems)
    setTotalPrice((currentPrice) => currentPrice + item.price)
  }

  const removeFromCart = (index: number) => {
    const price = cartItems[index].price
    const items = [...cartItems]
    items.splice(index, 1)
    console.log(items)
    setCartItems(items)
    setTotalPrice(totalPrice - price)
    ///setTotalPrice((currentPri) => currentPrice - price)
  }

  const emptyCart = () => {
    setCartItems([])
  }

  return (
    <Context.Provider
      value={{
        allItems,
        setAllItems,
        loading,
        setLoading,
        cartItems,
        emptyCart,
        addToCart,
        removeFromCart,
        totalPrice,
      }}
    >
      {children}
    </Context.Provider>
  )
}

export { ContextProvider, Context }
