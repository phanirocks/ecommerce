export interface Order {
    customerId: string,
    orderDate: string,
    paymentMethod: string,
    products: OrderItem[],
    status: string,
    totalPrice: number,
    currency: string,
    email: string,
    address: string,
    shippingCompany: string,
    shippingTrackingNumber: string,
    _id:string,
}

export interface OrderItem {
     productId: string,
     quantity: number
}

export interface CreateOrderRequest {
    date: string,
    products: OrderItem[],
    totalPrice: number,
    currency: string,
    email: string,
    address: string
}