export interface Product {
    _id: string
    name:string
    price: number
    imageURL: string
    description: string
    category: string
    quantity: number
    currency: string
}