import React, { useContext, useState } from 'react'
import './App.css'
import { SearchBar } from './components/SearchBar/SearchBar'
import { Context } from './Context/AppContext'
import { Product } from './types/item'
import { Item } from './components/Item/item'
import { AiOutlineShoppingCart } from 'react-icons/ai/index'
import { RiShoppingCart2Fill } from 'react-icons/ri/index'
import { AiFillShopping } from 'react-icons/ai/index'
import { Routes, Route, Link, useNavigate } from 'react-router-dom'
import { Cart } from './components/Cart/Cart'
import { ProductDetail } from './components/ProductDetail/ProductDetail'
import { Home } from './components/Home/Home'
import CategorySelector from './components/CategorySelector/CategorySelector'
import { CategoryPage } from './components/CategoryPage/CategoryPage'
import { HomeIcon } from '@heroicons/react/20/solid'
import DesktopSidebar from './components/DesktopSidebar/DesktopSidebar'
import logo from './E.png'
import { OrderPage } from './components/Order/OrderPage'
import { SearchResultsPage } from './components/SearchResultsPage/SearchResultsPage'

function App() {
  const { loading } = useContext(Context)

  const navigate = useNavigate()
  if (loading) {
    return <div className={'spinner'}>Loading..</div>
  } else {
    return (
      <div className="App">
        <header className="App-header shadow-lg">
          <img
            src={logo}
            className="w-20 h-10 cursor-pointer hidden md:block"
            onClick={() => navigate('/')}
          />
          <CategorySelector />

          {/*<Link to='/'><h4 className='header-logo'>Logo</h4></Link>*/}

          <div className="header-search">
            <SearchBar />
          </div>
          <div className="cart-container">
            <Link to="/cart">
              <AiOutlineShoppingCart size={32} />
            </Link>
          </div>
        </header>
        <main className="main flex flex-row py-12 align-center justify-center">
          <DesktopSidebar />
          <Routes>
            <Route
              path="/cart"
              element={
                <div className="cart-page">
                  <Cart />
                </div>
              }
            ></Route>
            <Route
              path="/product-details/:id/:title"
              element={<ProductDetail />}
            ></Route>
            <Route
              path="/category/:category"
              element={<CategoryPage />}
            ></Route>
            <Route
              path="/search-results/:searchTerm"
              element={<SearchResultsPage />}
            ></Route>
            <Route
              path="/orders/:id"
              element={
                <div className="cart-page">
                  <OrderPage />
                </div>
              }
            ></Route>
            <Route
              path="/error"
              element={
                <div className="cart-page">
                  An unexpected Error has occurred. Click on Home on the menu to
                  navigate back to the home page.
                </div>
              }
            ></Route>

            <Route path="/" element={<Home />}></Route>
          </Routes>
        </main>
        <footer className="footer">
          <div>@All Rights Reserved</div>
          <div className="px-8">
            <strong>Contact:</strong> phanirocks@gmail.com
          </div>
        </footer>
      </div>
    )
  }
}

export default App
